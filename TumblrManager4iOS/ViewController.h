//
//  ViewController.h
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 17..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *button;

@end

