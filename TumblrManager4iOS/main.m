//
//  main.m
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 17..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
