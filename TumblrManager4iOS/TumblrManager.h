//
//  TumblrManager.h
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 18..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TumblrToken : NSObject

@property (nonatomic, strong) NSString *consumerToken;
@property (nonatomic, strong) NSString *consumerSecret;
@property (nonatomic, strong) NSString *oauthToken;
@property (nonatomic, strong) NSString *oauthSecret;
@property (nonatomic, strong) NSString *oauthVerifier;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *accessSecret;

@end

typedef void (^ResultBlock) (TumblrToken *token);

@interface TumblrManager : NSObject

@property (nonatomic, strong) NSString *consumerKey;
@property (nonatomic, strong) NSString *consumerSecret;

+ (instancetype)sharedManager;

- (void)requestWithAuthViewController:(UIViewController *)viewController
                        consumerToken:(NSString *)consumerToken
                       consumerSecret:(NSString *)consumerSecret
                              success:(void (^)(TumblrToken *token))success;

- (void)requestAuthentication:(ResultBlock)result;

@end
