//
//  TumblrOAuth.h
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 18..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, HTTPMethod) {
    POST,
    GET,
    PUT,
    DELETE
};

static NSString* HTTPMethodToString(HTTPMethod method);

@class OAuthParameterArray;
@class OAuthAPIV1Request;

@interface OAuthAPIV1 : NSObject

@property (nonatomic, readonly) NSURL *requestTokenURL;
@property (nonatomic, readonly) NSURL *accessTokenURL;
@property (nonatomic, readonly) NSURL *authorizeURL;
@property (nonatomic, strong) NSString *consumerKey;
@property (nonatomic, strong) NSString *consumerSecret;
@property (nonatomic, readonly) NSString *callbackURLString;

- (id)initWithConsumerKey:(NSString *)consumerKey
           consumerSecret:(NSString *)consumerSecret
              callbackUrl:(NSString *)callbackUrl
          requestTokenURL:(NSURL *)requestTokenURL
           accessTokenURL:(NSURL *)accessTokenURL
             authorizeURL:(NSURL *)authorizeURL
                  success:(void (^)(BOOL succeed))success
                     fail:(void (^)(NSString *faileMessage, NSError *error))fail;

- (OAuthAPIV1Request *)post:(NSURL *)url
                 parameters:(OAuthParameterArray *)parameters;

- (OAuthAPIV1Request *)get:(NSURL *)url
                parameters:(OAuthParameterArray *)parameters;

- (OAuthAPIV1Request *)request:(NSURL *)url
                        method:(HTTPMethod)httpMethod
                    parameters:(OAuthParameterArray *)parameters;

- (OAuthAPIV1Request *)requestTokenWithHTTPMethod:(HTTPMethod)httpMethod;

@end

/**
 *  OAuthParameter
 *  container class for oauth authorized parameter
 *  constained string in this class always be normal string 
 *  that not encoded and not percentaged, not crypted
 */
@interface OAuthParameter : NSObject<NSCopying>

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *value;

+(instancetype)parameter:(NSString *)key value:(NSString *)value;
+(instancetype)parameterString:(NSString *)parameterString;

- (NSString *)encodedString;
- (NSString *)decodedString;
- (NSString *)encodedStringForRequestHeader;

@end

@interface OAuthParameterArray : NSObject<NSFastEnumeration, NSCopying>
{
    NSUInteger _count;
}
@property (nonatomic, strong) NSMutableArray *array;

- (NSUInteger)count;
- (void)addParameter:(OAuthParameter *)parameter;
- (void)addParameter:(NSString *)key value:(NSString *)value;
/**
 *
 *
 *  @param keyEqualValueParameterString this is must key=value string format
 */
- (void)addParameterString:(NSString*)keyEqualValueParameterString;
- (void)setParametersString:(NSString *)parametersString;
- (void)addParametersFromParameters:(OAuthParameterArray *)parameters;
- (OAuthParameter *)parameterAtIndex:(NSUInteger)index;
- (NSString *)signature:(NSURL *)url
             httpMethod:(NSString *)method
         consumerSecret:(NSString *)consumerSecret
      accessTokenSecret:(NSString *)accessTokenSecret;
- (NSString *)stringForURLQueryWithEncoding;

@end


@interface NSURL (OAuthAPIV1)

- (OAuthParameterArray *)parameters;
- (NSString *)normalizedForOAuthSignatureString;
- (NSURL *)URLByAddingCredentials:(NSURLCredential *)credential;

@end

@interface NSString (OAuthAPIV1)

- (NSString *)stringByURLEncoding;
- (NSString *)stringByURLDecoding;
+ (NSString *)random32CharactersForNonce;
- (NSString *)stringWithHMAC_SHA1_Signing:(NSString *)key;
- (OAuthParameterArray *)stringToParameters;

- (NSURL *)stringToURL;

@end

@interface OAuthAPIV1Request : NSMutableURLRequest

@property (nonatomic, strong) OAuthParameterArray *parameters;
@property (nonatomic, assign) HTTPMethod requestHTTPMethod;
@property (nonatomic, assign) BOOL isMediaUpload;

- (instancetype)initWithURL:(NSURL *)URL
                     method:(HTTPMethod)httpMethod
              isMediaUpload:(BOOL)isMediaUpload;
- (void)signedRequest:(OAuthAPIV1 *)api;
- (NSString *)stringForOAuthHeaderValueWithParameter:(OAuthParameterArray *)parameters;
- (void)sendAsynchronous:(void (^)(NSInteger statusCode, NSString *resultMessage, NSError *error))result;
- (void)sendSynchronous:(NSInteger *)statusCode result:(NSString **)resultMessage error:(NSError **)error;

- (void)addParameter:(NSString *)key value:(NSString *)value;

- (void)setURLCredential:(NSURLCredential *)credential;
- (void)setUsername:(NSString *)username password:(NSString *)password;

@end
