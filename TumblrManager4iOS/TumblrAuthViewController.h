//
//  TumblrAuthViewController.h
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 18..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAuthAPIV1.h"

typedef void (^AuthorizedBlock)(BOOL auhorized);

@interface TumblrAuthViewController : UIViewController

@property (nonatomic, strong) OAuthAPIV1 *api;

@property (nonatomic, readonly) BOOL authorized;
@property (nonatomic, assign) AuthorizedBlock authorizedBlock;
@property (nonatomic, strong) UIWebView *webView;

- (void)showAuthViewController:(UIViewController *)parentViewController
                    authorized:(AuthorizedBlock)authorizedBlock;

@end
