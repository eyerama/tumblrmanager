//
//  TumblrManager.m
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 18..
//  Copyright (c) 2014년 jay. All rights reserved.
//
#import "TumblrManager.h"
#import "TumblrAuthViewController.h"
#import "OAuthAPIV1.h"

const NSString *request_token_url = @"http://www.tumblr.com/oauth/request_token";
const NSString *access_token_url = @"http://www.tumblr.com/oauth/access_token";
const NSString *authorize_url = @"http://www.tumblr.com/oauth/authorize";

@implementation TumblrToken

@end

@interface TumblrManager ()

@property (nonatomic, strong) TumblrToken *token;
@property (nonatomic, assign) void (^SuccessBlock)(TumblrToken *token);
@property (nonatomic, weak) TumblrAuthViewController *authViewController;

@property (nonatomic, strong) OAuthAPIV1 *api;

@end

@implementation TumblrManager

+ (instancetype)sharedManager {
    static TumblrManager *sharedTumblrManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedTumblrManager = [[TumblrManager alloc] init];
    });
    return sharedTumblrManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.api =  [[OAuthAPIV1 alloc] initWithConsumerKey:nil
                                             consumerSecret:nil
                                                callbackUrl:@"projectg://tumblr-authorize"
                                            requestTokenURL:[request_token_url stringToURL]
                                             accessTokenURL:[access_token_url stringToURL]
                                               authorizeURL:[authorize_url stringToURL]
                                                    success:^(BOOL succeed) {
                                                        
                                                    }
                                                       fail:^(NSString *faileMessage, NSError *error) {
                                                           
                                                       }];
    }
    return self;
}

- (void)requestAuthentication:(ResultBlock)result {
    self.api.consumerKey = self.consumerKey;
    self.api.consumerSecret = self.consumerSecret;
    
    OAuthAPIV1Request *requestTokenRequest = [self.api requestTokenWithHTTPMethod:GET];
    [requestTokenRequest sendAsynchronous:^(NSInteger statusCode, NSString *resultMessage, NSError *error) {
        if (error || 200 != statusCode) {
            NSLog(@"%@ : %@", error.localizedDescription, [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
        }
        else {
            /**
             *  이 시점에서 authorization url에 파라미터로 들어갈 값을 resultMessage로 반환함
             */
            TumblrAuthViewController *authViewController = [[TumblrAuthViewController alloc] init];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
                [mainWindow addSubview:authViewController.view];
                authViewController.view.alpha = 0.0f;
                [UIView animateWithDuration:0.3
                                 animations:^{
                                     authViewController.view.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished) {
                                     authViewController.api = self.api;
                                     
                                     [authViewController.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:resultMessage
                                                                                                                 relativeToURL:self.api.authorizeURL]
                                                                                              cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                                                          timeoutInterval:30]];
                                 }];
            });
        }
    }];
}

@end
