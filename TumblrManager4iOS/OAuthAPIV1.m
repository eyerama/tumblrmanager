//
//  TumblrOAuth.m
//  TumblrManager4iOS
//
//  Created by jay on 2014. 11. 18..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import "OAuthAPIV1.h"
#import <CommonCrypto/CommonCrypto.h>
#import <UIKit/UIKit.h>

static NSString * HTTPMethodToString(HTTPMethod method) {
    switch (method) {
        case POST:
            return @"POST";
        case DELETE:
            return @"DELETE";
        case PUT:
            return @"PUT";
        default: // this is get
            return @"GET";
    }
    return @"GET";
}

#pragma mark - OAuthAPIV1 privates
@interface OAuthAPIV1 ()

@property (nonatomic, strong) NSString *oauthRequestToken;
@property (nonatomic, strong) NSString *oauthRequestTokenSecret;
@property (nonatomic, strong) NSString *oauthAccessToken;
@property (nonatomic, strong) NSString *oauthAccessSecret;
@property (nonatomic, assign) BOOL authorized;
@property (nonatomic, readonly) NSString *oauthSignatureMethod;
@property (nonatomic, readonly) NSString *oauthVersion;

@property (nonatomic, strong) NSURL *requestTokenURL;
@property (nonatomic, strong) NSURL *accessTokenURL;
@property (nonatomic, strong) NSURL *authorizeURL;
@property (nonatomic, strong) NSString *callbackURLString;

@property (nonatomic, strong) void (^SuccessBlock)(BOOL succeed);
@property (nonatomic, strong) void (^FailedBlock)(NSString *failMessage, NSError *error);

- (NSString *)oauthTimestamp;


@end

#pragma mark - NSString category
@implementation NSString (OAuthAPIV1)

- (NSString *)stringByURLEncoding {
    return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                  (CFStringRef)self,
                                                                                  NULL,
                                                                                  CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                  kCFStringEncodingUTF8);
}

- (NSString*)stringByURLDecoding {
    return (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapes(NULL, (CFStringRef)self,
                                                                                    CFSTR(""));
}

+ (NSString *)randomStringForNonce {
    CFUUIDRef cfuuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuid = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, cfuuid);
    CFRelease(cfuuid);
    return uuid;
}

+ (NSString *)random32CharactersForNonce {
//    NSString *randomString = [NSString randomStringForNonce];
//    NSParameterAssert(32 <= randomString.length);
//    return [randomString substringToIndex:32];
    return [[NSProcessInfo processInfo] globallyUniqueString];
}

- (NSString *)stringWithHMAC_SHA1_Signing:(NSString *)key {
    unsigned char buf[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, key.UTF8String, key.length, self.UTF8String, self.length, buf);
    NSData *signedData = [NSData dataWithBytes:buf length:CC_SHA1_DIGEST_LENGTH];
#warning 인증이 실패한다면 base64 인코딩 옵션 문제일 가능성이 있음
    return [signedData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (OAuthParameterArray *)stringToParameters {
    OAuthParameterArray *array = [[OAuthParameterArray alloc] init];
    [array setParametersString:self];
    return array;
}

- (NSURL *)stringToURL {
    return [NSURL URLWithString:self];
}

@end

#pragma mark - NSURL Category
@implementation NSURL (OAuthAPIV1)

- (OAuthParameterArray *)parameters {
    NSString *query = [self query];
    NSArray *parameters = [query componentsSeparatedByString:@"&"];
    OAuthParameterArray *parameterArray = [[OAuthParameterArray alloc] init];
    
    for (NSString *parameter in parameters) {
        [parameterArray addParameterString:parameter];
    }
    
    return parameterArray;
}

- (NSString *)normalizedForOAuthSignatureString {
    return [NSString stringWithFormat:@"%@://%@%@", [self scheme], [self host], [self path]];
}

- (NSURL *)URLByAddingCredentials:(NSURLCredential *)credential {
    if (nil == credential) {
        return nil;
    }
    NSString *scheme = [self scheme];
    NSString *host = [self host];
    
    // alert be credentials settings in this url
    if ([host rangeOfString:@"@"].location != NSNotFound) {
        return self;
    }
    NSMutableString *resourceSpecifier = [[self resourceSpecifier] mutableCopy];
    if ([resourceSpecifier hasPrefix:@"//"] == NO) {
        return nil;
    }
    NSString *userPassword = [NSString stringWithFormat:@"%@:%@", credential.user, credential.password];
    [resourceSpecifier insertString:userPassword
                            atIndex:2];
    NSString *urlString = [NSString stringWithFormat:@"%@:%@", scheme, resourceSpecifier];
    
    return [NSURL URLWithString:urlString];
}

@end

#pragma mark - OAuthParameter implement
@implementation OAuthParameter

+(instancetype)parameter:(NSString *)key value:(NSString *)value {
    OAuthParameter *p = [[OAuthParameter alloc] init];
    p.key = key;
    p.value = value;
    return p;
}

+(instancetype)parameterString:(NSString *)parameterString {
    NSArray *parameter = [parameterString componentsSeparatedByString:@"="];
    NSParameterAssert(2 == parameter.count);
    OAuthParameter *p = [[OAuthParameter alloc] init];
    p.key = [parameter[0] stringByURLDecoding];
    p.value = [parameter[1] stringByURLDecoding];
    return p;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@=%@", self.key, self.value];
}

- (NSString *)encodedString {
    return [NSString stringWithFormat:@"%@=%@", [self.key stringByURLEncoding], [self.value stringByURLEncoding]];
}

- (NSString *)decodedString {
    return [NSString stringWithFormat:@"%@=%@", [self.key stringByURLDecoding], [self.value stringByURLDecoding]];
}

- (NSString *)encodedStringForRequestHeader {
    return [NSString stringWithFormat:@"%@=\"%@\"", [self.key stringByURLEncoding], [self.value stringByURLEncoding]];
}

- (instancetype)copyWithZone:(NSZone *)zone {
    OAuthParameter *copiedParameter = [[[self class] alloc] init];
    if (copiedParameter) {
        copiedParameter.key = [self.key copyWithZone:zone];
        copiedParameter.value = [self.value copyWithZone:zone];
    }
    return copiedParameter;
}

@end

#pragma mark - OAuthParameterArray implement
@implementation OAuthParameterArray

- (id)init {
    self = [super init];
    if (self) {
        self.array = [NSMutableArray new];
    }
    return self;
}

- (NSUInteger)count {
    return self.array.count;
}

- (void)addParameter:(OAuthParameter *)parameter {
    OAuthParameter *temp_p = nil;
    for (OAuthParameter *p in self.array) {
        if ([p.key isEqualToString:parameter.key]) {
            temp_p = p;
            break;
        }
    }
    if (temp_p) {
        temp_p.value = parameter.value;
    }
    else {
        [self.array addObject:parameter];
        _count = self.array.count;
    }
}

- (void)addParameter:(NSString *)key value:(NSString *)value {
    [self addParameter:[OAuthParameter parameter:key
                                           value:value]];
}

- (void)addParameterString:(NSString *)keyEqualValueParameterString {
    [self addParameter:[OAuthParameter parameterString:keyEqualValueParameterString]];
}

- (void)setParametersString:(NSString *)parametersString {
    NSArray *parameters = [parametersString componentsSeparatedByString:@"&"];
    self.array = [NSMutableArray new];
    _count = 0;
    for (NSString *parameter in parameters) {
        [self addParameterString:parameter];
    }
}

- (void)addParametersFromParameters:(OAuthParameterArray *)parameters {
    for (OAuthParameter *parameter in parameters) {
        [self addParameter:parameter];
    }
}

- (OAuthParameter *)parameterAtIndex:(NSUInteger)index {
    return self.array[index];
}

#pragma mark - NSFastEnumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state
                                  objects:(__unsafe_unretained id [])buffer
                                    count:(NSUInteger)len {
//    NSUInteger bufferIndex = 0;
//    
//    NSUInteger listIndex = state->state;
//    NSUInteger listLength = _count;
//    
//    while (bufferIndex < len) {
//        // 배열 종료
//        if (listIndex >= listLength) {
//            break;
//        }
//        
//        buffer[bufferIndex++] = self.array[listIndex++];
//    }
//    
//    state->state = listIndex;
//    state->itemsPtr = buffer;
//    state->mutationsPtr = (unsigned long *)(__bridge void *)self; // 오브젝트를 구조체의 포인터에 집어넣기 위한 캐스트
//    
//    return bufferIndex;
    return [self.array countByEnumeratingWithState:state
                                           objects:buffer
                                             count:len];
}

- (NSString *)signature:(NSURL *)url
             httpMethod:(NSString *)method
                    consumerSecret:(NSString *)consumerSecret
      accessTokenSecret:(NSString *)accessTokenSecret {
    
    OAuthParameterArray *queries = [url parameters];
    OAuthParameterArray *tempParams = [self copy];
    [tempParams addParametersFromParameters:queries];
    
    NSString *head = method.uppercaseString;
    NSString *host = [NSString stringWithFormat:@"%@://%@%@", url.scheme, url.host, url.path];
    NSString *query = [tempParams stringForURLQueryWithEncoding];
    NSLog(@"%@", head);
    NSLog(@"%@", host);
    NSLog(@"%@", query);
    NSString *signatureBaseString = [NSString stringWithFormat:@"%@&%@&%@",
                                     head,
                                     [host stringByURLEncoding],
                                     [query stringByURLEncoding]];
    
    NSString *encodedConsumerSecret = [consumerSecret stringByURLEncoding];
    NSString *encodedTokenSecret = [accessTokenSecret stringByURLEncoding];
    
    NSString *signingKey = [NSString stringWithFormat:@"%@&%@", encodedConsumerSecret, encodedTokenSecret ? encodedTokenSecret : @""];

    
    return [signatureBaseString stringWithHMAC_SHA1_Signing:signingKey];
}

- (NSString *)stringForURLQueryWithEncoding {
    NSMutableArray *queries = [NSMutableArray new];
    
    NSArray *sortedArray = [self.array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[obj1 key] caseInsensitiveCompare:[obj2 key]];
    }];
    for (OAuthParameter *p in sortedArray) {
        [queries addObject:[p encodedString]];
    }
    
    return [queries componentsJoinedByString:@"&"];
}

- (NSString *)stringForURLQuery {
    NSMutableArray *queries = [NSMutableArray new];
    
    NSArray *sortedArray = [self.array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[obj1 key] compare:[obj2 key]];
    }];
    for (OAuthParameter *p in sortedArray) {
        [queries addObject:p];
    }
    
    return [queries componentsJoinedByString:@"&"];
}

- (id)copyWithZone:(NSZone *)zone {
    OAuthParameterArray *copiedArray = [[[self class] alloc] init];
    if (copiedArray) {
        for (OAuthParameter *parameter in self) {
            [copiedArray addParameter:[parameter copyWithZone:zone]];
        }
    }
    return copiedArray;
}

- (NSString *)description {
    return [self.array description];
}

@end

#pragma mark - OAuthAPIV1Request implement
@interface OAuthAPIV1Request ()

@property (nonatomic, strong) OAuthAPIV1 *api;

+ (NSMutableDictionary *)sharedCredential;
+ (NSMutableArray *)localCookieStorages;

@end
@implementation OAuthAPIV1Request

- (instancetype)initWithURL:(NSURL *)URL
                        api:(OAuthAPIV1 *)api
                     method:(HTTPMethod)httpMethod
              isMediaUpload:(BOOL)isMediaUpload {
    
    self = [super initWithURL:URL
                  cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
              timeoutInterval:30];
    if (self) {
        self.api = api;
        self.requestHTTPMethod = httpMethod;
        self.isMediaUpload = isMediaUpload;
    }
    
    return self;
}

- (void)signedRequest:(OAuthAPIV1 *)api {
    NSParameterAssert(nil != api);
    NSParameterAssert(nil != api.consumerKey);
    NSParameterAssert(nil != api.consumerSecret);
    
    OAuthParameterArray *parameters = [[OAuthParameterArray alloc] init];
    [parameters addParameter:@"oauth_consumer_key" value:api.consumerKey];
    if (api.oauthAccessToken) { // this is authorized request
        [parameters addParameter:@"oauth_token" value:api.oauthAccessToken];
    }
    else {
        if (api.oauthRequestToken) { // this is request token request
            [parameters addParameter:@"oauth_token" value:api.oauthRequestToken];
        }
    }
    [parameters addParameter:@"oauth_signature_method" value:[api oauthSignatureMethod]];
    [parameters addParameter:@"oauth_timestamp" value:[api oauthTimestamp]];
    [parameters addParameter:@"oauth_nonce" value:[NSString random32CharactersForNonce]];
    [parameters addParameter:@"oauth_version" value:[api oauthVersion]];
    
    NSString *httpMethod = HTTPMethodToString(self.requestHTTPMethod);
    OAuthParameterArray *oauthParameters = [parameters copy];
    if (self.parameters) {
        [oauthParameters addParametersFromParameters:self.parameters];
    }
    OAuthParameterArray *oauthAndPostAndGETParameters = [self.URL parameters];
    [oauthAndPostAndGETParameters addParametersFromParameters:oauthParameters];
    
    NSString *signature = [self.isMediaUpload ? parameters : oauthAndPostAndGETParameters
                                               signature:self.URL
                                              httpMethod:httpMethod
                                             consumerSecret:api.consumerSecret
                                       accessTokenSecret:api.oauthAccessSecret];
    
    [parameters addParameter:@"oauth_signature"
                       value:signature];
    
    NSMutableArray *queries = [NSMutableArray new];
    for (OAuthParameter *p in parameters) {
        [queries addObject:[NSString stringWithFormat:@"%@=\"%@\"", p.key, [p.value stringByURLEncoding]]];
    }
    [self addValue:[NSString stringWithFormat:@"OAuth %@",[queries componentsJoinedByString:@","]]
forHTTPHeaderField:@"Authorization"];
}

- (NSString *)stringForOAuthHeaderValueWithParameter:(OAuthParameterArray *)parameters {
    NSMutableArray *encodedParameters = [NSMutableArray new];
    for (OAuthParameter *parameter in parameters) {
        [encodedParameters addObject:[parameter encodedStringForRequestHeader]];
    }
    NSString *encodedParametersString = [encodedParameters componentsJoinedByString:@", "];
    NSString *headerValue = [NSString stringWithFormat:@"Oauth %@", encodedParametersString];
    return headerValue;
}

- (void)addParameter:(NSString *)key value:(NSString *)value {
    if (nil == self.parameters) {
        self.parameters = [[OAuthParameterArray alloc] init];
    }
    [self.parameters addParameter:key
                            value:value];
}

- (void)sendAsynchronous:(void (^)(NSInteger, NSString *, NSError *))result {
    __strong __block id weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSInteger statusCode;
        NSString *resultMessage;
        NSError *error;
        [weakSelf sendSynchronous:&statusCode
                           result:&resultMessage
                            error:&error];
        if (result) {
            result(statusCode, resultMessage, error);
        }
        weakSelf = nil;
    });
}

- (void)sendSynchronous:(NSInteger *)statusCode
                  result:(NSString *__autoreleasing *)resultMessage
                   error:(NSError *__autoreleasing *)error {
    
    NSURL *sendURL = self.URL;

    [self setHTTPMethod:HTTPMethodToString(self.requestHTTPMethod)];
    
    switch (self.requestHTTPMethod) {
        case POST:
            if (self.parameters) {
                
                CFStringEncoding cfStringEncoding = CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding);
                NSString *encodingName = (NSString *)CFStringConvertEncodingToIANACharSetName(cfStringEncoding);
                if (encodingName) {
                    [self setValue:[NSString stringWithFormat:@"application/x-www-form-urlencoded; charset=%@", encodingName]
                forHTTPHeaderField:@"Content-Type"];
                }
                
                NSString *parameterString = [self.parameters stringForURLQueryWithEncoding];
                NSData *parameterData = [parameterString dataUsingEncoding:NSUTF8StringEncoding
                                                      allowLossyConversion:YES];
                [self setValue:[NSString stringWithFormat:@"%lu", (unsigned long)parameterData.length]
            forHTTPHeaderField:@"Content-Length"];
                [self setHTTPBody:parameterData];
            }
            break;
        case GET:
            if (self.parameters) {
                NSString *queryString = [self.parameters stringForURLQueryWithEncoding];
                if (nil != queryString && 0 < queryString.length) {
                    NSString *prevQuery = sendURL.query;
                    NSString *query = @"";
                    if (0 < prevQuery.length) {
                        query = [query stringByAppendingString:prevQuery];
                        query = [query stringByAppendingString:@"&"];
                    }
                    [query stringByAppendingString:queryString];
                    sendURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@%@?%@",
                                                    sendURL.scheme,
                                                    sendURL.host,
                                                    sendURL.path,
                                                    queryString ]];
                    self.URL = sendURL;
                }
            }
            break;
        default:
            break;
    }
    
    [self signedRequest:self.api];
    
    NSLog(@"%@", [self valueForHTTPHeaderField:@"Authorization"]);
    
    NSHTTPURLResponse *response = nil;

    NSData *responseData = [NSURLConnection sendSynchronousRequest:self
                                                 returningResponse:&response
                                                             error:error];
    *statusCode = response.statusCode;
    *resultMessage = [NSString stringWithUTF8String:responseData.bytes];
}

+ (NSMutableDictionary *)sharedCredential {
    static NSMutableDictionary *sharedCredential;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCredential = [NSMutableDictionary new];
    });
    return sharedCredential;
}

+ (NSMutableArray *)localCookieStorages {
    static NSMutableArray *localCookieStorages;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        localCookieStorages = [NSMutableArray new];
    });
    return localCookieStorages;
}

- (void)setURLCredential:(NSURLCredential *)credential {
    [[OAuthAPIV1Request sharedCredential] setObject:credential
                                             forKey:[self.URL host]];
}

- (void)setUsername:(NSString *)username password:(NSString *)password {
    NSURLCredential *credential = [NSURLCredential credentialWithUser:username
                                                             password:password
                                                          persistence:NSURLCredentialPersistenceNone];
    [self setURLCredential:credential];
}

@end

#pragma mark - OAuthAPIV1 implement
@implementation OAuthAPIV1

- (id)initWithConsumerKey:(NSString *)consumerKey
           consumerSecret:(NSString *)consumerSecret
              callbackUrl:(NSString *)callbackUrl
          requestTokenURL:(NSURL *)requestTokenURL
           accessTokenURL:(NSURL *)accessTokenURL
             authorizeURL:(NSURL *)authorizeURL
                  success:(void (^)(BOOL succeed))success
                     fail:(void (^)(NSString *failedMessage, NSError *error))fail
{
    self = [super init];
    if (self) {
        self.consumerKey = consumerKey;
        self.consumerSecret = consumerSecret;
        self.oauthRequestToken = nil;
        self.oauthRequestTokenSecret = nil;
        self.oauthAccessToken = nil;
        self.oauthAccessSecret = nil;
        self.authorized = NO;
        self->_oauthSignatureMethod = @"HMAC-SHA1";
        self->_oauthVersion = @"1.0";
        
        self.requestTokenURL = requestTokenURL;
        self.accessTokenURL = accessTokenURL;
        self.authorizeURL = authorizeURL;
        self.callbackURLString = callbackUrl;
        
        self.SuccessBlock = success;
        self.FailedBlock = fail;
    }
    return self;
}

- (NSString *)oauthTimestamp {
    return [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
}

- (OAuthAPIV1Request *)requestTokenWithHTTPMethod:(HTTPMethod)httpMethod {
    OAuthAPIV1Request *request = nil;
    switch (httpMethod) {
        case POST:
            request = [self post:self.requestTokenURL
                      parameters:nil];
            break;
        default:
            request = [self get:self.requestTokenURL
                     parameters:nil];
            break;
    }
    return request;
}

- (OAuthAPIV1Request *)post:(NSURL *)url
                 parameters:(OAuthParameterArray *)parameters {
    return [self request:url
                  method:POST
              parameters:parameters];
}

- (OAuthAPIV1Request *)get:(NSURL *)url
                parameters:(OAuthParameterArray *)parameters {
    return [self request:url
                  method:GET
              parameters:parameters];
}

- (OAuthAPIV1Request *)request:(NSURL *)url
                        method:(HTTPMethod)httpMethod
                    parameters:(OAuthParameterArray *)parameters {
    OAuthAPIV1Request *request = [[OAuthAPIV1Request alloc] initWithURL:url
                                                                    api:self
                                                                 method:httpMethod
                                                          isMediaUpload:NO];
    request.parameters = parameters;
    return request;
}

@end
